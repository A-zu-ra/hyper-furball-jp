package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class FallingMushroom extends Bullet {
		protected var hasHit:Boolean = false;
		
		[Embed(source='../img/mushroomspin.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../img/mushroomspin2.png')]
		private static const kImage2File:Class;
		private var image2:Image = new Image(kImage2File);
		
		[Embed(source='../img/mushroomspin3.png')]
		private static const kImage3File:Class;
		private var image3:Image = new Image(kImage3File);
		
		private var anglemod:int = -1;
		
		public function FallingMushroom() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = halfHeight;
			image2.originX = halfWidth;
			image2.originY = halfHeight;
			image3.originX = halfWidth;
			image3.originY = halfHeight;
			setHitbox(image.width * 0.75, image.height * 0.75);
			originX = halfWidth;
			originY = halfHeight;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, level:int):void {
			super.init(x, y, level);
			hasHit = false;
			image.alpha = 1;
			image2.alpha = 1;
			image3.alpha = 1;
			var ra:int = FP.rand(3);
			if (ra == 0) {
				graphic = image;
			} else if (ra == 1) {
				graphic = image2;
			} else {
				graphic = image3;
			}
			anglemod = FP.rand(2) * 2 - 1;
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}
			
			y += GameWorld.difficulty == 1 ? 8 : 6;
			image.angle += 5 * anglemod;
			image2.angle += 5 * anglemod;
			image3.angle += 5 * anglemod;

			super.update();

			if (y > 300) {
				FP.world.recycle(this);
			}
		}
		
		override protected function onPlayerCollide(player:Player):void {
			if (!hasHit) {
				if (level > 20) {
					player.hit(550);
				} else {
					player.hit(200);
				}
				hasHit = true;
			}
		}
	}
}
