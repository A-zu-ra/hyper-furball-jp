package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class EndStoryWorld extends World {
		[Embed(source='../mus/endstory.mp3')]
		private static const kMusFile:Class;
		private static var mus:Sfx = new Sfx(kMusFile);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);

		[Embed(source='../img/endstory1.png')]
		private static const kStory1ImageFile:Class;
		private var story1Image:Image = new Image(kStory1ImageFile);
		[Embed(source='../img/endstory2.png')]
		private static const kStory2ImageFile:Class;
		private var story2Image:Image = new Image(kStory2ImageFile);
		[Embed(source='../img/endstory3.png')]
		private static const kStory3ImageFile:Class;
		private var story3Image:Image = new Image(kStory3ImageFile);
		
		[Embed(source='../img/JPendstory1.png')]
		private static const jpKStory1ImageFile:Class;
		private var jpStory1Image:Image = new Image(jpKStory1ImageFile);
		[Embed(source='../img/JPendstory2.png')]
		private static const jpKStory2ImageFile:Class;
		private var jpStory2Image:Image = new Image(jpKStory2ImageFile);
		[Embed(source='../img/JPendstory3.png')]
		private static const jpKStory3ImageFile:Class;
		private var jpStory3Image:Image = new Image(jpKStory3ImageFile);
		
		private var storyImages:Vector.<Image> = new <Image> [
			story1Image, story2Image, story3Image
		];
		private var jpStoryImages:Vector.<Image> = new <Image> [
			jpStory1Image, jpStory2Image, jpStory3Image
		];

		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/meow.mp3')]
		private static const kMeowSfxFile:Class;
		private var meowSfx:Sfx = new Sfx(kMeowSfxFile);

		[Embed(source='../sfx/levelup.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var fadeTimer:int = -1;

		private static const kFadeRate:Number = 1.0 / 30.0;
		
		private var currentImage:int = 0;
		
		private var spaceText:Text;

		public function EndStoryWorld() {
			FP.screen.color = 0x000000;

			if (MenuWorld.language == 1) {
				for each (var jpStoryImage:Image in jpStoryImages) {
					addGraphic(jpStoryImage);
					jpStoryImage.alpha = 0;
				}
			} else {
				for each (var storyImage:Image in storyImages) {
					addGraphic(storyImage);
					storyImage.alpha = 0;
				}
			}
			
			if (MenuWorld.language == 1) {
				spaceText = new Text("SPACEキーを押して続ける", FP.halfWidth, FP.height - 30);
				spaceText.font = "defaultjpn";
			} else {
				spaceText = new Text("Press Space to Continue", FP.halfWidth, FP.height - 30);
			}
			spaceText.centerOO();
			spaceText.centerOrigin();
			addGraphic(spaceText);

			mus.loop();
		}

		override public function update():void {
			super.update();

			if (Input.pressed(Key.ENTER) || Input.pressed("attack")) {
				if (MenuWorld.language == 1) {
					if (currentImage < jpStoryImages.length) {
						if (currentImage == 1) {
							meowSfx.play();
						} else if (currentImage == jpStoryImages.length - 1) {
							confirmSfx.play();
						} else {
							cursorSfx.play();
						}
						currentImage++;
					}
				} else {
					if (currentImage < storyImages.length) {
						if (currentImage == 1) {
							meowSfx.play();
						} else if (currentImage == storyImages.length - 1) {
							confirmSfx.play();
						} else {
							cursorSfx.play();
						}
						currentImage++;
					}
				}
			}
			
			if (MenuWorld.language == 1) {
				for each (var jpStoryImage:Image in jpStoryImages) {
					jpStoryImage.alpha -= kFadeRate;
				}
				if (currentImage < jpStoryImages.length) {
					jpStoryImages[currentImage].alpha += kFadeRate * 2;
				}
				if (currentImage == 2) {
					jpStoryImages[1].alpha = 1;
					jpStoryImages[2].alpha = 1;
				}

				if (currentImage == 0) {
					spaceText.alpha = jpStoryImages[0].alpha;
				}
				
				if (currentImage > 0 && currentImage < jpStoryImages.length) {
					spaceText.alpha += 1.0 / 60.0;
				}
				
				if (currentImage == jpStoryImages.length) {
					spaceText.alpha = jpStoryImages[currentImage - 1].alpha;
					mus.volume = jpStoryImages[currentImage - 1].alpha;
					if (jpStoryImages[currentImage - 1].alpha <= 0.0) {
						FP.world = new EndWorld;
					}
				}
			} else {
				for each (var storyImage:Image in storyImages) {
					storyImage.alpha -= kFadeRate;
				}
				if (currentImage < storyImages.length) {
					storyImages[currentImage].alpha += kFadeRate * 2;
				}
				if (currentImage == 2) {
					storyImages[1].alpha = 1;
					storyImages[2].alpha = 1;
				}

				if (currentImage == 0) {
					spaceText.alpha = storyImages[0].alpha;
				}
				
				if (currentImage > 0 && currentImage < storyImages.length) {
					spaceText.alpha += 1.0 / 60.0;
				}
				
				if (currentImage == storyImages.length) {
					spaceText.alpha = storyImages[currentImage - 1].alpha;
					mus.volume = storyImages[currentImage - 1].alpha;
					if (storyImages[currentImage - 1].alpha <= 0.0) {
						FP.world = new EndWorld;
					}
				}
			}
		}
	}
}
