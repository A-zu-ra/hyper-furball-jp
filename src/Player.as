package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.Screen;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Text;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Player extends Entity {
		private static const kHealthbarWidth:int = 100;
		private static const kHealthbarHeight:int = 10;

		public var healthBar:Image = Image.createRect(kHealthbarWidth, kHealthbarHeight, 0x00FF00);
		public var healthBarBackground:Image = Image.createRect(kHealthbarWidth + 2, kHealthbarHeight + 2, 0x000000);

		public var experienceBar:Image = Image.createRect(kHealthbarWidth, kHealthbarHeight, 0xFFFF00);
		public var experienceBarBackground:Image = Image.createRect(kHealthbarWidth + 2, kHealthbarHeight + 2, 0x000000);

		public var nameText:Text;
		public var jpNameText:Text;
		public var experienceText:Text;
		public var healthText:Text;
		
		public var health:int = maxHealth();
		
		public var experience:int = 0;
		public var level:int = 1;
		public var levelPoints:int = 0;
		
		private static var currentCameraX:Number = 200;
		private static const kCameraY:Number = GameWorld.kGroundY;
		
		[Embed(source='../img/player.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 153, 72);
		
		[Embed(source='../sfx/player_attack.mp3')]
		private static const kAttackSfxFile:Class;
		private var attackSfx:Sfx = new Sfx(kAttackSfxFile);

		[Embed(source='../sfx/player_hurt.mp3')]
		private static const kHurtSfxFile:Class;
		private var hurtSfx:Sfx = new Sfx(kHurtSfxFile);

		[Embed(source='../sfx/levelup.mp3')]
		private static const kLevelupSfxFile:Class;
		private var levelupSfx:Sfx = new Sfx(kLevelupSfxFile);

		[Embed(source='../sfx/player_block.mp3')]
		private static const kBlockSfxFile:Class;
		private var blockSfx:Sfx = new Sfx(kBlockSfxFile);

		[Embed(source='../sfx/player_blocked.mp3')]
		private static const kBlockedSfxFile:Class;
		private var blockedSfx:Sfx = new Sfx(kBlockedSfxFile);

		[Embed(source='../img/particlestar.png')]
		private static const kParticleFile:Class;

		private var emitter:Emitter;

		// Cooldowns/animation timers.
		private var anythingTimer:int = 0;
		
		public function Player(x:Number, y:Number) {
			addAnimations();
			
			super(x, y, spritemap);

			setHitbox(spritemap.width, spritemap.height);
			spritemap.originX = 120;
			spritemap.originY = height;
			originX = 120;
			originY = height;
			
			type = "player";
			name = "player";
			layer = -100;
			
			spritemap.play("stand");

			addGraphic(healthBarBackground);
			healthBarBackground.y = 35;
			healthBarBackground.x = -40;
			healthBarBackground.originX = healthBarBackground.width / 2;
			healthBarBackground.originY = healthBarBackground.height / 2;
			
			addGraphic(healthBar);
			healthBar.y = 35;
			healthBar.x = -40;
			healthBar.x -= healthBar.width / 2;
			healthBar.originY = healthBar.height / 2;
			
			addGraphic(experienceBarBackground);
			experienceBarBackground.y = 46;
			experienceBarBackground.x = -40;
			experienceBarBackground.originX = experienceBarBackground.width / 2;
			experienceBarBackground.originY = experienceBarBackground.height / 2;
			
			addGraphic(experienceBar);
			experienceBar.y = 46;
			experienceBar.x = -40;
			experienceBar.x -= experienceBar.width / 2;
			experienceBar.originY = experienceBar.height / 2;
			
			nameText = new Text("Furball", 0, 20);
			nameText.color = 0xFFFFFF;
			nameText.x = -105;
			nameText.originX = 0;
			nameText.originY = nameText.height / 2;
			jpNameText = new Text("ファーボール", 0, 20);
			jpNameText.font = "defaultjpn";
			jpNameText.color = 0xFFFFFF;
			jpNameText.x = -105;
			jpNameText.originX = 0;
			jpNameText.originY = jpNameText.height / 2;
			if (MenuWorld.language == 1) { addGraphic(jpNameText); } else { addGraphic(nameText); }

			experienceText = new Text("10", -40, 46);
			experienceText.color = 0xFFFFFF;
			experienceText.size = 12;
			experienceText.originY = experienceText.textHeight / 2;
			addGraphic(experienceText);

			healthText = new Text("10", -40, 35);
			healthText.color = 0xFFFFFF;
			healthText.size = 12;
			healthText.originY = healthText.textHeight / 2;
			addGraphic(healthText);

			// Particles.
			emitter = new Emitter(kParticleFile, 32, 32);
			for each (var particleType:String in ["red", "green", "blue", "yellow", "cyan", "magenta"]) {
				emitter.newType(particleType, [0]);
				emitter.setAlpha(particleType, 1, 0);
				emitter.setMotion(particleType, 135, 128, 16, 90, 256, 32);
			}
			
			emitter.setColor("red", 0xFF8080, 0xFF8080);
			emitter.setColor("green", 0x80FF80, 0x80FF80);
			emitter.setColor("blue", 0x8080FF, 0x8080FF);
			emitter.setColor("yellow", 0xFFFF80, 0xFFFF80);
			emitter.setColor("magenta", 0xFF80FF, 0xFF80FF);
			emitter.setColor("cyan", 0x80FFFF, 0x80FFFF);
			emitter.y = kCameraY - FP.height;
			emitter.x = -currentCameraX;
			addGraphic(emitter);
		}
		
		public function activateHyperMode():void {
			health = maxHealth();
		}
		
		override public function update():void {
			super.update();
			
			if (level >= 15) {
				if (currentCameraX < 350) {
					currentCameraX++;
				}
			}
			
			if (GameWorld.paused) {
				return;
			}
			
			handleContinue();
			
			anythingTimer++;
			
			nameText.text = "Furball (Level " + level + ")";
			jpNameText.text = "ファーボール (lv" + level + ")";
			
			if (spritemap.complete) {
				spritemap.play("stand");
			}
			
			// Movement.
			handleMovement();
			
			// Attacking and blocking.
			handleAttacking();
			handleBlocking();
			
			handleHyperMode();
			handleHurt();
			
			// Healthbar, experience bar.
			var healthPercent:Number = (health as Number) / maxHealth();
			healthBar.scaleX = healthPercent;
			var experiencePercent:Number = (experience as Number) / experienceNeeded();
			experienceBar.scaleX = experiencePercent;
			experienceText.text = experience.toString() + "/" + experienceNeeded();
			experienceText.originX = experienceText.textWidth / 2;
			healthText.text = health.toString() + "/" + maxHealth();
			healthText.originX = healthText.textWidth / 2;
			
			// Update camera.
			FP.setCamera(x - currentCameraX, y - FP.height + kCameraY);
			
			// update scratches.
			var vec:Vector.<ScratchBullet> = new Vector.<ScratchBullet>;
			FP.world.getClass(ScratchBullet, vec);
			for each (var arst:ScratchBullet in vec) {
				arst.updatePos();
			}
		}
		
		private function handleContinue():void {
			if (spritemap.currentAnim == "dead" && Input.pressed(Key.ENTER)) {
				health = maxHealth();
				levelupSfx.play();
				var screenFlash:ScreenFlash = FP.world.create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 1.0, 0.02);
				spritemap.play("stand");
				GameWorld.continuesUsed++;
			}
		}
		
		private function handleHurt():void {
			if (spritemap.currentAnim == "hurt" && anythingTimer % 2 == 0) {
				spritemap.alpha = 0;
			} else {
				spritemap.alpha = 1;
			}
		}
		
		public function getExperience(amount:int):void {
			if (Upgrade.experienceUpgrade().owned) {
				amount *= 1.25;
			}
			
			experience += amount;
			while (experience >= experienceNeeded()) {
				experience -= experienceNeeded();
				level++;
				levelPoints++;
				if (MenuWorld.language == 1) {
					Messages.message("レベルアップ！ファーボールはレベル" + level + "になった！");
				} else {
					Messages.message("Level Up!  You are now level " + level + "!");
				}
				var damageNumber:DamageNumber = FP.world.create(DamageNumber) as DamageNumber;
				damageNumber.reset(x, y, "LEVEL UP!", 0x80FF80);
				levelupSfx.play();
				GameWorld.world().levelUp(level);
				health = maxHealth();
				var screenFlash:ScreenFlash = FP.world.create(ScreenFlash) as ScreenFlash;
				screenFlash.reset(0xFFFFFF, 1.0, 0.02);
			}
		}
		
		private function handleHyperMode():void {
			// Particles.
			if (GameWorld.world().isHyperMode() && spritemap.currentAnim != "block") {
				spritemap.play("hyper");
				for each (var particleType:String in ["red", "green", "blue", "yellow", "cyan", "magenta"]) {
					if (anythingTimer % 4 == 0) {
						emitter.emit(particleType, FP.random * FP.width, FP.random * FP.height);
					}
				}
			} else {
				if (spritemap.currentAnim == "hyper") {
					spritemap.play("stand");
				}
			}
		}
		
		private function tryMove(amount:Number):Boolean {
			// Try moving in x.
			var prevX:Number = x;
			x += amount;
			
			// Check for enemy collisions.
			var enemy:Enemy;
			var tries:int = 0;
			while ((enemy = collide("enemy", x, y) as Enemy) != null) {
				if (Upgrade.hyperPushUpgrade().owned && GameWorld.world().isHyperMode() && tries == 0) {
					enemy.x += 16;
					tries++;
					continue;
				}
				x = prevX;
				return false;
			}
			
			return true;
		}
		
		private function handleMovement():void {
			var xVel:Number = 0.0;
			
			if (Input.check(Key.LEFT)) {
				xVel -= movespeed();
			}
			if (Input.check(Key.RIGHT)) {
				xVel += movespeed();
			}
			if (spritemap.currentAnim == "walk" && xVel == 0) {
				spritemap.play("stand");
			}
			
			// Hyper mode automatically moves you forwards.
			if (GameWorld.world().isHyperMode()) {
				tryMove(hyperMovespeed());
				return;
			}
			
			// Can only move from standing or moving.
			if (spritemap.currentAnim != "stand" && spritemap.currentAnim != "walk") {
				return;
			}
			if (xVel == 0.0) {
				return;
			}
			
			if (tryMove(xVel)) {
				spritemap.play("walk");
			} else {
				spritemap.play("stand");
			}
		}
		
		private function handleAttacking():void {
			if (MenuWorld.language == 1) {
				if (GameWorld.world().jpInstructions1Image.visible) {
					return;
				}
			} else {
				if (GameWorld.world().instructions1Image.visible) {
					return;
				}
			}
			
			if (Input.pressed("attack") ||
			    (Upgrade.hyperRapidUpgrade1().owned && GameWorld.world().isHyperMode() && anythingTimer % 6 == 0) ||
				(Upgrade.hyperRapidUpgrade2().owned && GameWorld.world().isHyperMode() && anythingTimer % 2 == 0)) {
				if (!GameWorld.world().isHyperMode() && spritemap.currentAnim != "walk" && spritemap.currentAnim != "stand") {
					return;
				}
				if (spritemap.currentAnim == "block") {
					return;
				}
				attackSfx.play();
				spritemap.play(attackAnimation());
				
				// Spawn bullet.
				var scratchBullet:ScratchBullet = FP.world.create(ScratchBullet) as ScratchBullet;
				scratchBullet.reset(x + 60, y + 15, level, Upgrade.attackRangeUpgrade().owned);
			}
		}
		
		private function handleBlocking():void {
			// Block tinting.
			if (spritemap.currentAnim == "block") {
				spritemap.color = 0xFFFFFF;
				spritemap.tintMode = Image.TINTING_COLORIZE;
				if (anythingTimer % 2 == 0) {
					spritemap.tinting = 0.5;
				} else {
					spritemap.tinting = 0;
				}
			} else {
				spritemap.tinting = 0;
			}
			
			if (Input.check("block")) {
				if (spritemap.currentAnim != "walk" && spritemap.currentAnim != "stand" && spritemap.currentAnim != "hyper") {
					return;
				}

				spritemap.play("block");
				blockSfx.play();
			} else {
				if (spritemap.currentAnim == "block") {
					spritemap.play("stand");
				}
			}
		}
		
		private function addAnimations():void {
			spritemap.add("stand", [0], 0.15);
			spritemap.add("walk", [0, 1, 2, 1], 0.15);
			spritemap.add("attack", [1], .25 / 3, false);
			spritemap.add("hyper", [5, 6, 7, 8], 0.5);
			spritemap.add("fastattack", [1], .25 / 3 * 2, false);
			spritemap.add("block", [3], 0.01);
			spritemap.add("hurt", [4], 0.03, false);
			spritemap.add("dead", [9], 0.03);
		}
		
		private function movespeed():Number {
			return Upgrade.speedUpgrade().owned ? 8.0 : 4.0;
		}
		
		private function hyperMovespeed():Number {
			return Upgrade.hyperSpeedUpgrade().owned ? movespeed() * 8 : movespeed() * 4;
		}
		
		private function attackAnimation():String {
			var result:String = "attack";
			if (Upgrade.attackSpeedUpgrade().owned) {
				result = "fast" + result;
			}
			return result;
		}
		
		private function maxHealth():int {
			return Upgrade.maxHealth();
		}
		
		public function getHealth():void {
			var amount:Number = maxHealth() / 5;
			if (Upgrade.heartUpgrade().owned) {
				amount *= 2;
			}
			var damageNumber:DamageNumber = FP.world.create(DamageNumber) as DamageNumber;
			damageNumber.reset(x, y, amount.toString(), 0x00ff00);
			if (MenuWorld.language == 1) {
				Messages.message("魚の骨を食った！ＨＰが" + amount + "回復！");
			} else {
				Messages.message("You ate some fish bones!  " + amount + " health restored!");
			}

			health += amount;
			if (health > maxHealth()) {
				health = maxHealth();
			}
		}
		
		public function hit(damage:int):void {
			var originalDamage:int = damage;
			if (spritemap.currentAnim == "block" || GameWorld.world().isHyperMode()) {
				GameWorld.world().blockHits++;
				
				damage -= blockDefense();
				if (damage < 0) {
					damage = 0;
				}
				if (GameWorld.world().isHyperMode() && damage > 1 && Upgrade.hyperDefenseUpgrade().owned) {
					damage = 1;
				}
				if (spritemap.currentAnim == "block" && GameWorld.world().isHyperMode() && damage > 1) {
					damage = 1;
				}
			} else {
				GameWorld.world().blockTimer = 32;
			}
			
			health -= damage;
			if (health < 0) {
				health = 0;
			}
			var damageNumber:DamageNumber;			
			if (spritemap.currentAnim == "block" || GameWorld.world().isHyperMode()) {
				if (MenuWorld.language == 1) {
					Messages.message("ファーボールに" + damage + "ダメージを与えた！（" + (originalDamage - damage) + "がブロックした）");
				} else {
					Messages.message("You were hit for " + damage + " damage!  (" + (originalDamage - damage) + " damage blocked)");
				}
				blockedSfx.play();
				damageNumber = FP.world.create(DamageNumber) as DamageNumber;
				damageNumber.reset(x, y, ( -damage).toString(), 0x808080);
			} else {
				if (MenuWorld.language == 1) {
					Messages.message("ファーボールに" + damage + "ダメージを与えた！");
				} else {
					Messages.message("You were hit for " + damage + " damage!");
				}
				hurtSfx.play();
				damageNumber = FP.world.create(DamageNumber) as DamageNumber;
				damageNumber.reset(x, y, ( -damage).toString(), 0xff0000);
			}
			if (!GameWorld.world().isHyperMode() && spritemap.currentAnim != "block") {
				spritemap.play("hurt");
			}

			if (health == 0) {
				die();
			}
		}
		
		public function die():void {
			GameWorld.world().deactivateHyperMode();
			spritemap.play("dead");
		}
		
		public function experienceNeeded():int {
			return Math.pow(1.2, level - 1) * 50 * level + 50;
		}
		
		public static function experienceNeededForLevel(levela:int):int {
			return Math.pow(1.2, levela - 1) * 50 * levela + 50;
		}
		
		public function progress():Number {
/*			var exp:Number = 0;
			for (var i:int = 1; i < level; ++i) {
				exp += experienceNeededForLevel(i);
			}

			exp += experience;
			
			var expNeeded:Number = 0;
			for (var j:int = 1; j < 15; ++j) {
				expNeeded += experienceNeededForLevel(j);
			}
			
			if (exp > expNeeded) {
				exp = expNeeded;
			}
			
			return exp / expNeeded;*/
			var result:Number = (level - 1) / 14.0;
			result += (experience as Number) / experienceNeeded() / 14.0;
			if (result > 1.0) {
				return 1.0;
			}
			return result;
		}
		
		public function blockDefense():int {
			return Upgrade.blockUpgrade().owned ? 500 : 100;
		}
	}
}
