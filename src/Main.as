package {
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	[Frame(factoryClass="Preloader")]
	
	public class Main extends Engine {
		public function Main() {
			super(800, 600, 60, true);
			
			// Settings.
			Input.define("attack", Key.SPACE);
			Input.define("block", Key.SHIFT);
			
			// Debug console.
			//FP.console.enable();

			// Start game.
			FP.world = new IntroWorld;
			//FP.world = new GameWorld;
			//FP.world = new BreaktimeWorld;
			//FP.world = new EndWorld;
			//FP.world = new MenuWorld;
		}
	}
}
