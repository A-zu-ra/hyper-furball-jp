package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Spritemap;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	
	// [A-zu-ra] This World file won't exist once the TL is done; DDR will figure out a way to do the toggle via the main menu.
	public class LanguageWorld extends World {
		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Backdrop = new Backdrop(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Backdrop = new Backdrop(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Backdrop = new Backdrop(backgroundFile3);

		[Embed(source='../img/background2 layer 1.png')]
		private static const background2File1:Class;
		private var background21:Backdrop = new Backdrop(background2File1);
		[Embed(source='../img/background2 layer 2.png')]
		private static const background2File2:Class;
		private var background22:Backdrop = new Backdrop(background2File2);
		[Embed(source='../img/background2 layer 3.png')]
		private static const background2File3:Class;
		private var background23:Backdrop = new Backdrop(background2File3);
		[Embed(source='../img/background2 layer 4.png')]
		private static const background2File4:Class;
		private var background24:Backdrop = new Backdrop(background2File4);

		[Embed(source='../img/background 3 layer 1.png')]
		private static const background3File1:Class;
		private var background31:Backdrop = new Backdrop(background3File1);
		[Embed(source='../img/background 3 layer 2.png')]
		private static const background3File2:Class;
		private var background32:Backdrop = new Backdrop(background3File2);
		[Embed(source='../img/background 3 layer 3.png')]
		private static const background3File3:Class;
		private var background33:Backdrop = new Backdrop(background3File3);
		[Embed(source='../img/background 3 layer 4.png')]
		private static const background3File4:Class;
		private var background34:Backdrop = new Backdrop(background3File4);

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		private var blackImage2:Image = new Image(kBlackImageFile);
		
		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);
		
		public var language:int = 1;

		private var fadeTimer:int = -1;

		private var anythingTimer:int = 0;

		private static const kFadeDuration:Number = 60;

		public function LanguageWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			background1.scrollX = 0.6;
			background2.scrollX = 0.8;
			background3.scrollX = 1.2;
			background1.scrollY = 0.0;
			background2.scrollY = 0.0;
			background3.scrollY = 0.0;
			background1.alpha = 0;
			background2.alpha = 0;
			background3.alpha = 0;

			addGraphic(background21);
			addGraphic(background22);
			addGraphic(background23);
			addGraphic(background24);
			background21.scrollX = 0.4;
			background22.scrollX = 0.6;
			background23.scrollX = 0.8;
			background24.scrollX = 1.2;
			background21.scrollY = 0.0;
			background22.scrollY = 0.0;
			background23.scrollY = 0.0;
			background24.scrollY = 0.0;
			background21.alpha = 0;
			background22.alpha = 0;
			background23.alpha = 0;
			background24.alpha = 0;

			addGraphic(background31);
			addGraphic(background32);
			addGraphic(background33);
			addGraphic(background34);
			background31.scrollX = 0.4;
			background32.scrollX = 0.6;
			background33.scrollX = 0.8;
			background34.scrollX = 1.2;
			background31.scrollY = 0.0;
			background32.scrollY = 0.0;
			background33.scrollY = 0.0;
			background34.scrollY = 0.0;
			background31.alpha = 0;
			background32.alpha = 0;
			background33.alpha = 0;
			background34.alpha = 0;
			
			blackImage2.alpha = 0.5;
			addGraphic(blackImage2);

			var selectText:Text = new Text("Select game language", FP.halfWidth, FP.halfHeight - 100);
			selectText.scale = 2
			selectText.originX = selectText.textWidth / 2;
			addGraphic(selectText);
			
			var jpSelectText:Text = new Text("言語を選択してください", FP.halfWidth, FP.halfHeight - 60);
			jpSelectText.font = "defaultjpn";
			jpSelectText.originX = jpSelectText.textWidth / 2;
			addGraphic(jpSelectText);
			
			var englishText:Text = new Text("English", FP.halfWidth - 160, FP.halfHeight + 50);
			englishText.scale = 2;
			englishText.originX = englishText.textWidth / 2;
			addGraphic(englishText);
			
			var enConfirmText:Text = new Text("Press Left key", FP.halfWidth - 160, FP.halfHeight + 90);
			enConfirmText.originX = enConfirmText.textWidth / 2;
			addGraphic(enConfirmText);
			
			var japaneseText:Text = new Text("日本語", FP.halfWidth + 160, FP.halfHeight + 50);
			japaneseText.font = "defaultjpn";
			japaneseText.size = 32;
			japaneseText.originX = japaneseText.textWidth / 2;
			addGraphic(japaneseText);
			
			var jpConfirmText:Text = new Text("右の矢印キーを押して", FP.halfWidth + 160, FP.halfHeight + 90);
			jpConfirmText.font = "defaultjpn";
			jpConfirmText.originX = jpConfirmText.textWidth / 2;
			addGraphic(jpConfirmText);

			blackImage.alpha = 1;
			addGraphic(blackImage);
			
			anythingTimer += FP.rand(3600);
		}

		override public function update():void {
			super.update();

			anythingTimer++;

			var rate:Number = -2;
			background1.x += rate * background1.scrollX;
			background2.x += rate * background2.scrollX;
			background3.x += rate * background3.scrollX;
			background21.x += rate * background21.scrollX;
			background22.x += rate * background22.scrollX;
			background23.x += rate * background23.scrollX;
			background24.x += rate * background24.scrollX;
			background31.x += rate * background31.scrollX;
			background32.x += rate * background32.scrollX;
			background33.x += rate * background33.scrollX;
			background34.x += rate * background34.scrollX;
			
			var fadeRate:Number = 1.0 / 60.0;
			background1.alpha -= fadeRate;
			background2.alpha -= fadeRate;
			background3.alpha -= fadeRate;
			background21.alpha -= fadeRate;
			background22.alpha -= fadeRate;
			background23.alpha -= fadeRate;
			background24.alpha -= fadeRate;
			background31.alpha -= fadeRate;
			background32.alpha -= fadeRate;
			background33.alpha -= fadeRate;
			background34.alpha -= fadeRate;
			if (anythingTimer % 3600 < 1200) {
				background1.alpha += fadeRate * 2;
				background2.alpha += fadeRate * 2;
				background3.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 2400) {
				background21.alpha += fadeRate * 2;
				background22.alpha += fadeRate * 2;
				background23.alpha += fadeRate * 2;
				background24.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 3600) {
				background31.alpha += fadeRate * 2;
				background32.alpha += fadeRate * 2;
				background33.alpha += fadeRate * 2;
				background34.alpha += fadeRate * 2;
			}

			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
				return;
			} else if (fadeTimer == 0) {
                FP.world = new MenuWorld;
				return;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;

			if (Input.check(Key.LEFT)) {
				MenuWorld.language = 0;
				confirmSfx.play();
				fadeTimer = kFadeDuration;
			}
			if (Input.check(Key.RIGHT)) {
				MenuWorld.language = 1;
				confirmSfx.play();
				fadeTimer = kFadeDuration;
			}
		}
	}
}
