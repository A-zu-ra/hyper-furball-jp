package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class BossEnemy extends Enemy {
		[Embed(source='../sfx/baby_die.mp3')]
		private static const kDieSfxFile:Class;

		[Embed(source='../sfx/baby_fire.mp3')]
		private static const kFireSfxFile:Class;
		private var fireSfx:Sfx = new Sfx(kFireSfxFile);

		[Embed(source='../img/boss.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 333, 443);
		
		private var bulletsFired:int = 0;
		
		private var bulletsFiredMilk:int = 0;
		
		protected override function dieSfx():Sfx {
			GameWorld.babiesKilled++;
			GameWorld.world().winGame();
			return new Sfx(kDieSfxFile);
		}
		
		protected override function cooldown():int {
			return GameWorld.difficulty == 1 ? 200 * 0.66 : 200;
		}
		
		protected override function experience():int {
			return 9001;
		}
		
		protected override function maxHealth():int {
			return 800.0 * 100 * level / 15.0;
		}
		
		protected override function enemyName():String {
			if (MenuWorld.language == 1) {
				return "悪のキノコベイビー";
			} else {
				return "Evil Mushroom Baby";
			}
		}
		
		public function BossEnemy() {
			setHitbox(spritemap.width, spritemap.height);
			spritemap.originX = halfWidth;
			spritemap.originY = height;
			setHitbox(spritemap.width - 250, spritemap.height);
			originX = halfWidth;
			originY = height;
			
			spritemap.add("stand", [0], 0.25);
			spritemap.add("attack", [1], 0.01, false);
			spritemap.play("stand");
			addGraphic(spritemap);
		}
		
		public function reset(x:Number, y:Number):void {
			super.init(x, y);
		}
		
		override protected function onFire():void {
			spritemap.play("attack");
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}

			var player:Player = FP.world.getInstance("player");
			if (Math.abs(x - player.x) > FP.width) {
				// Don't update off-screen enemies.
				return;
			}

			super.update();
			
//			if (cooldownTimer == cooldown() - 30) {
	//			fireSfx.play();
		//	}
			
			if (GameWorld.world().isHyperMode()) {
				bulletsFiredMilk = 0;
			}
		
			if (cooldownTimer == cooldown() - 1 ||
				cooldownTimer == cooldown() - 31 ||
				cooldownTimer == cooldown() - 61) {
				// Spawn bullets.
				
				if (bulletsFiredMilk % 30 == 24) {
					var babyBulleta:FallingMilk = FP.world.create(FallingMilk) as FallingMilk;
					babyBulleta.reset(x - FP.random * 400 - 80, -FP.height + 200, level);
				} else if (bulletsFired % 10 == 9) {
					var babyBulletb:FallingBone = FP.world.create(FallingBone) as FallingBone;
					babyBulletb.reset(x - FP.random * 400 - 80, -FP.height + 200, level);
				} else {
					var babyBulletc:FallingMushroom = FP.world.create(FallingMushroom) as FallingMushroom;
					babyBulletc.reset(x - FP.random * 500, -FP.height + 200, level);
				}
				fireSfx.play();
				bulletsFired++;
				bulletsFiredMilk++;
			}
			
			if (spritemap.complete) {
				spritemap.play("stand");
			}
		}
		
		override protected function sprite():Spritemap {
			return spritemap;
		}
	}
}
