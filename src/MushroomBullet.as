package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class MushroomBullet extends Bullet {
		private static const kDuration:int = 30;
		private static const kFadeDuration:int = 10;
		protected var hasHit:Boolean = false;
		
		[Embed(source='../img/mushroombullet.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		public function MushroomBullet() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = height;
			originX = halfWidth;
			originY = height;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number, level:int):void {
			super.init(x, y, level);
			hasHit = false;
			image.alpha = 1;
		}
		
		override public function update():void {
			x -= 4;
			super.update();
			if (bulletTimer > kDuration - kFadeDuration) {
				image.alpha = 1.0 - ((bulletTimer - (kDuration - kFadeDuration)) as Number) / kFadeDuration;
			}
			if (bulletTimer == kDuration) {
				FP.world.recycle(this);
			}
		}
		
		override protected function onPlayerCollide(player:Player):void {
			if (!hasHit) {
				player.hit(10 * level);
				hasHit = true;
			}
		}
	}
}