package {
	import flash.display.DisplayObjectContainer;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Backdrop;
	import net.flashpunk.graphics.Emitter;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class EndWorld extends World {
		[Embed(source='../mus/story.mp3')]
		private static const kMusFile:Class;
		private static var mus:Sfx = new Sfx(kMusFile);

		[Embed(source='../img/background layer 1.png')]
		private static const backgroundFile1:Class;
		private var background1:Backdrop = new Backdrop(backgroundFile1);
		[Embed(source='../img/background layer 2.png')]
		private static const backgroundFile2:Class;
		private var background2:Backdrop = new Backdrop(backgroundFile2);
		[Embed(source='../img/background layer 3.png')]
		private static const backgroundFile3:Class;
		private var background3:Backdrop = new Backdrop(backgroundFile3);

		[Embed(source='../img/background2 layer 1.png')]
		private static const background2File1:Class;
		private var background21:Backdrop = new Backdrop(background2File1);
		[Embed(source='../img/background2 layer 2.png')]
		private static const background2File2:Class;
		private var background22:Backdrop = new Backdrop(background2File2);
		[Embed(source='../img/background2 layer 3.png')]
		private static const background2File3:Class;
		private var background23:Backdrop = new Backdrop(background2File3);
		[Embed(source='../img/background2 layer 4.png')]
		private static const background2File4:Class;
		private var background24:Backdrop = new Backdrop(background2File4);

		[Embed(source='../img/background 3 layer 1.png')]
		private static const background3File1:Class;
		private var background31:Backdrop = new Backdrop(background3File1);
		[Embed(source='../img/background 3 layer 2.png')]
		private static const background3File2:Class;
		private var background32:Backdrop = new Backdrop(background3File2);
		[Embed(source='../img/background 3 layer 3.png')]
		private static const background3File3:Class;
		private var background33:Backdrop = new Backdrop(background3File3);
		[Embed(source='../img/background 3 layer 4.png')]
		private static const background3File4:Class;
		private var background34:Backdrop = new Backdrop(background3File4);

		private var anythingTimer:int = 0;

		[Embed(source='../img/black.png')]
		private static const kBlackImageFile:Class;
		private var blackImage:Image = new Image(kBlackImageFile);
		private var blackImage2:Image = new Image(kBlackImageFile);

		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private var fadeTimer:int = -1;
		
		private var statsText:Text = new Text("", FP.halfWidth, FP.halfHeight);

		private static const kFadeDuration:Number = 60;

		public function EndWorld() {
			FP.screen.color = 0x000000;
			
			addGraphic(background1);
			addGraphic(background2);
			addGraphic(background3);
			background1.scrollX = 0.6;
			background2.scrollX = 0.8;
			background3.scrollX = 1.2;
			background1.scrollY = 0.0;
			background2.scrollY = 0.0;
			background3.scrollY = 0.0;
			background1.alpha = 0;
			background2.alpha = 0;
			background3.alpha = 0;

			addGraphic(background21);
			addGraphic(background22);
			addGraphic(background23);
			addGraphic(background24);
			background21.scrollX = 0.4;
			background22.scrollX = 0.6;
			background23.scrollX = 0.8;
			background24.scrollX = 1.2;
			background21.scrollY = 0.0;
			background22.scrollY = 0.0;
			background23.scrollY = 0.0;
			background24.scrollY = 0.0;
			background21.alpha = 0;
			background22.alpha = 0;
			background23.alpha = 0;
			background24.alpha = 0;

			addGraphic(background31);
			addGraphic(background32);
			addGraphic(background33);
			addGraphic(background34);
			background31.scrollX = 0.4;
			background32.scrollX = 0.6;
			background33.scrollX = 0.8;
			background34.scrollX = 1.2;
			background31.scrollY = 0.0;
			background32.scrollY = 0.0;
			background33.scrollY = 0.0;
			background34.scrollY = 0.0;
			background31.alpha = 0;
			background32.alpha = 0;
			background33.alpha = 0;
			background34.alpha = 0;
			
			blackImage2.alpha = 0.5;
			addGraphic(blackImage2);
			
			anythingTimer += FP.rand(3600);
			
			var titleText:Text = new Text("Game Over", FP.halfWidth, 50);
			titleText.originX = titleText.textWidth / 2;
			var jpTitleText:Text = new Text("ゲーム終了", FP.halfWidth, 50);
			jpTitleText.font = "defaultjpn";
			jpTitleText.originX = jpTitleText.textWidth / 2;
			if (MenuWorld.language == 1) { addGraphic(jpTitleText); } else { addGraphic(titleText); }
			
			var seconds:int = (GameWorld.totalTime / 60.0) % 60;
			var minutes:int = (GameWorld.totalTime / 60.0 / 60);
			statsText.y = 100;
			if (MenuWorld.language == 1) {
				statsText.font = "defaultjpn";
				statsText.text = "コンティニュー　" + GameWorld.continuesUsed + "数" +
				"\n倒したキノコ　　" + GameWorld.mushroomsKilled + "個" +
				"\n倒した泣き虫　　" + GameWorld.babiesKilled + "子" +
				"\n食った魚の骨　　" + GameWorld.bonesFound + "個" +
				"\n見つけたミルク　" + GameWorld.milkFound + "本" +
				"\n攻撃数　　　　　" + GameWorld.attacksUsed + "回" +
				"\n\n成功時間　　　　" + minutes + "分" + (seconds < 10 ? "0" : "") + seconds + "秒" +
				"\n\n使ったスキル";
				
				if (GameWorld.difficulty == 0) {
					statsText.text = "ノーマルモード\n" + statsText.text;
				} else {
					statsText.text = "ハードモード\n" + statsText.text;
				}
				
				for each (var jpUpgrade:Upgrade in Upgrade.jpUpgrades) {
					if (jpUpgrade.owned) {
						statsText.text += "\n" + jpUpgrade.name;
					}
				}
				statsText.originX = statsText.textWidth / 2;
				addGraphic(statsText);
			} else {
				statsText.text = "Continues used: " + GameWorld.continuesUsed +
				"\nMushrooms destroyed: " + GameWorld.mushroomsKilled +
				"\nBabies scratched: " + GameWorld.babiesKilled +
				"\nBones eaten: " + GameWorld.bonesFound +
				"\nMilk bottles found: " + GameWorld.milkFound +
				"\nAttack count: " + GameWorld.attacksUsed +
				"\n\nTotal time: " + minutes + ":" + (seconds < 10 ? "0" : "") + seconds +
				"\n\nUpgrades:";
				
				if (GameWorld.difficulty == 0) {
					statsText.text = "Normal Mode\n" + statsText.text;
				} else {
					statsText.text = "Hard Mode\n" + statsText.text;
				}
				
				for each (var upgrade:Upgrade in Upgrade.upgrades) {
					if (upgrade.owned) {
						statsText.text += "\n" + upgrade.name;
					}
				}
				statsText.originX = statsText.textWidth / 2;
				addGraphic(statsText);
			}
			
			var continueText:Text = new Text("Press Enter to Continue", FP.halfWidth, 520);
			continueText.originX = continueText.textWidth / 2;
			var jpContinueText:Text = new Text("ENTERキーを押して続ける", FP.halfWidth, 520);
			jpContinueText.font = "defaultjpn";
			jpContinueText.originX = jpContinueText.textWidth / 2;
			if (MenuWorld.language == 1) { addGraphic(jpContinueText); } else { addGraphic(continueText); }
			
			blackImage.alpha = 1;
			addGraphic(blackImage);
			
			mus.volume = 1;
			mus.loop();
		}

		override public function update():void {
			super.update();

			anythingTimer++;

			var rate:Number = -2;
			background1.x += rate * background1.scrollX;
			background2.x += rate * background2.scrollX;
			background3.x += rate * background3.scrollX;
			background21.x += rate * background21.scrollX;
			background22.x += rate * background22.scrollX;
			background23.x += rate * background23.scrollX;
			background24.x += rate * background24.scrollX;
			background31.x += rate * background31.scrollX;
			background32.x += rate * background32.scrollX;
			background33.x += rate * background33.scrollX;
			background34.x += rate * background34.scrollX;
			
			var fadeRate:Number = 1.0 / 60.0;
			background1.alpha -= fadeRate;
			background2.alpha -= fadeRate;
			background3.alpha -= fadeRate;
			background21.alpha -= fadeRate;
			background22.alpha -= fadeRate;
			background23.alpha -= fadeRate;
			background24.alpha -= fadeRate;
			background31.alpha -= fadeRate;
			background32.alpha -= fadeRate;
			background33.alpha -= fadeRate;
			background34.alpha -= fadeRate;
			if (anythingTimer % 3600 < 1200) {
				background1.alpha += fadeRate * 2;
				background2.alpha += fadeRate * 2;
				background3.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 2400) {
				background21.alpha += fadeRate * 2;
				background22.alpha += fadeRate * 2;
				background23.alpha += fadeRate * 2;
				background24.alpha += fadeRate * 2;
			} else if (anythingTimer % 3600 < 3600) {
				background31.alpha += fadeRate * 2;
				background32.alpha += fadeRate * 2;
				background33.alpha += fadeRate * 2;
				background34.alpha += fadeRate * 2;
			}

			if (fadeTimer > 0) {
				fadeTimer--;
				blackImage.alpha = 1.0 - fadeTimer / kFadeDuration;
				mus.volume = fadeTimer / kFadeDuration;
				return;
			} else if (fadeTimer == 0) {
				mus.stop();
                FP.world = new MenuWorld;
				return;
			}
			
			blackImage.alpha -= 1.0 / kFadeDuration;

			if (Input.pressed(Key.ENTER)) {
				// Return to main menu.
				confirmSfx.play();
				fadeTimer = kFadeDuration;
			}
		}
	}
}
