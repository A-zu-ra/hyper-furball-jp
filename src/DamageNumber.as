package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class DamageNumber extends Entity {
		private const kFadeRate:Number = 0.1;
		private const kScrollRate:Number = 4.0;
		private const kDuration:int = 20;
		private const kVariance:int = 40;
		
		private var text:Text;
		
		private var timer:int;
		
		public function reset(x:Number, y:Number, amount:String, color:uint):void {
			x += FP.rand(kVariance) - kVariance / 2;
			y += FP.rand(kVariance) - kVariance / 2;
			text = new Text(amount, x, y);
			text.scale = 2;
			text.centerOrigin();
			text.color = color;
			graphic = text;
			
			layer = -1000;
			
			timer = kDuration;
		}
		
		override public function update():void {
			if (GameWorld.paused) {
				return;
			}

			text.y -= kScrollRate;
			
			if (timer > 0) {
				timer--;
				return;
			}
			
			text.alpha -= kFadeRate;
			if (text.alpha <= 0) {
				world.recycle(this);
			}
		}
	}
}
