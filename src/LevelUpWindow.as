package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Spritemap;

	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class LevelUpWindow extends Entity {
		// Used to disable pressing enter on the first frame.
		private var firstFrame:Boolean = true;

		[Embed(source='../sfx/levelup.mp3')]
		private static const kLevelupSfxFile:Class;
		private var levelupSfx:Sfx = new Sfx(kLevelupSfxFile);

		[Embed(source='../sfx/cursor.mp3')]
		private static const kCursorSfxFile:Class;
		private var cursorSfx:Sfx = new Sfx(kCursorSfxFile);

		[Embed(source='../sfx/error.mp3')]
		private static const kErrorSfxFile:Class;
		private var errorSfx:Sfx = new Sfx(kErrorSfxFile);

		[Embed(source='../mus/store.mp3')]
		private static const kStoreSfxFile:Class;
		private var storeSfx:Sfx = new Sfx(kStoreSfxFile);

		[Embed(source='../img/levelupwindow.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);

		[Embed(source='../img/cursor.png')]
		private static const kCursorImageFile:Class;
		private var cursorImage:Image = new Image(kCursorImageFile);

		[Embed(source='../img/checkbox.png')]
		private static const kCheckboxFile:Class;

		[Embed(source='../img/checkmark.png')]
		private static const kCheckmarkFile:Class;
		private var checkmarks:Vector.<Image> = new Vector.<Image>();
		
		[Embed(source='../sfx/confirm.mp3')]
		private static const kConfirmSfxFile:Class;
		private var confirmSfx:Sfx = new Sfx(kConfirmSfxFile);

		private static const kUpgradeStartX:int = 100;
		private static const kUpgradeStartY:int = 100;
		private static const kUpgradeWidth:int = 200;
		private static const jpKUpgradeWidth:int = 250; // [A-zu-ra} Reserved because ファーボールハイパーフレンジ overshoots the English checkboxes.
		private static const kUpgradeHeight:int = 20;

		private var selectedIndex:int = 0;

		private var upgradeNameTexts:Vector.<Text> = new Vector.<Text>();
		private var upgradeNameDescription:Text = new Text("");
		private var upgradeDescription:Text = new Text("");
		private var upgradeLevelDescription:Text = new Text("");
		private var levelPointsText:Text = new Text("");
		private var levelText:Text = new Text("");
		private var progressText:Text = new Text("");
		private var distanceText:Text = new Text("");
		
		private var jpUpgradeNameDescription:Text = new Text("");
		private var jpUpgradeDescription:Text = new Text("");
		private var jpUpgradeLevelDescription:Text = new Text("");
		private var jpLevelPointsText:Text = new Text("");
		private var jpLevelText:Text = new Text("");
		private var jpProgressText:Text = new Text("");
		private var jpDistanceText:Text = new Text("");
		
		[Embed(source='../img/skill.png')]
		private static const kSpritemapFile:Class;
		private var spritemap:Spritemap = new Spritemap(kSpritemapFile, 300, 200);

		public function LevelUpWindow() {
			super(0, 0, image);
			visible = false;
			layer = -2000;
			image.scrollX = 0;
			image.scrollY = 0;

			if (MenuWorld.language == 1) {
				for (var j:int = 0; j < Upgrade.upgrades.length; ++j) {
					var jpText:Text = new Text(Upgrade.jpUpgrades[j].name, kUpgradeStartX, kUpgradeHeight * j + kUpgradeStartY);
					jpText.font = "defaultjpn";
					jpText.color = 0xffffff;
					jpText.scrollX = 0;
					jpText.scrollY = 0;
					upgradeNameTexts.push(jpText);
					addGraphic(jpText);

					var jpCheckbox:Image = new Image(kCheckboxFile);
					jpCheckbox.scrollX = 0;
					jpCheckbox.scrollY = 0;
					jpCheckbox.x = kUpgradeStartX + jpKUpgradeWidth;
					jpCheckbox.y = kUpgradeStartY + kUpgradeHeight * j;
					addGraphic(jpCheckbox);

					var jpCheckmark:Image = new Image(kCheckmarkFile);
					jpCheckmark.scrollX = 0;
					jpCheckmark.scrollY = 0;
					jpCheckmark.x = kUpgradeStartX + jpKUpgradeWidth;
					jpCheckmark.y = kUpgradeStartY + kUpgradeHeight * j;
					addGraphic(jpCheckmark);
					checkmarks.push(jpCheckmark);
				}
			} else {
				for (var i:int = 0; i < Upgrade.upgrades.length; ++i) {
					var text:Text = new Text(Upgrade.upgrades[i].name, kUpgradeStartX, kUpgradeHeight * i + kUpgradeStartY);
					text.color = 0xffffff;
					text.scrollX = 0;
					text.scrollY = 0;
					upgradeNameTexts.push(text);
					addGraphic(text);

					var checkbox:Image = new Image(kCheckboxFile);
					checkbox.scrollX = 0;
					checkbox.scrollY = 0;
					checkbox.x = kUpgradeStartX + kUpgradeWidth;
					checkbox.y = kUpgradeStartY + kUpgradeHeight * i;
					addGraphic(checkbox);

					var checkmark:Image = new Image(kCheckmarkFile);
					checkmark.scrollX = 0;
					checkmark.scrollY = 0;
					checkmark.x = kUpgradeStartX + kUpgradeWidth;
					checkmark.y = kUpgradeStartY + kUpgradeHeight * i;
					addGraphic(checkmark);
					checkmarks.push(checkmark);
				}
			}
			var returnText:Text = new Text("Return to Game", kUpgradeStartX, kUpgradeHeight * Upgrade.upgrades.length + kUpgradeStartY);
			returnText.color = 0xffffff;
			returnText.scrollX = 0;
			returnText.scrollY = 0;
			var jpReturnText:Text = new Text("ゲームに戻る", kUpgradeStartX, kUpgradeHeight * Upgrade.upgrades.length + kUpgradeStartY);
			jpReturnText.font = "defaultjpn";
			jpReturnText.color = returnText.color;
			jpReturnText.scrollX = returnText.scrollX;
			jpReturnText.scrollY = returnText.scrollY;
			if (MenuWorld.language == 1) { addGraphic(jpReturnText); } else { addGraphic(returnText); }

			var titleText:Text = new Text("Skills", FP.halfWidth, 60);
			titleText.scale = 2;
			titleText.scrollX = 0;
			titleText.scrollY = 0;
			titleText.color = 0xffff00;
			titleText.originX = titleText.textWidth / 2;
			var jpTitleText:Text = new Text("スキル", FP.halfWidth, 60);
			jpTitleText.font = "defaultjpn";
			jpTitleText.size = 32;
			jpTitleText.scrollX = titleText.scrollX;
			jpTitleText.scrollY = titleText.scrollY;
			jpTitleText.color = titleText.color;
			jpTitleText.originX = titleText.originX;
			if (MenuWorld.language == 1) { addGraphic(jpTitleText); } else { addGraphic(titleText); }
			
			upgradeDescription.x = 400;
			upgradeDescription.y = 380;
			upgradeDescription.scrollX = 0;
			upgradeDescription.scrollY = 0;
			upgradeDescription.color = 0xffffff;
			upgradeDescription.wordWrap = true;
			upgradeDescription.width = 300;
			jpUpgradeDescription.font = "defaultjpn";
			jpUpgradeDescription.x = 400;
			jpUpgradeDescription.y = 380;
			jpUpgradeDescription.scrollX = 0;
			jpUpgradeDescription.scrollY = 0;
			jpUpgradeDescription.color = 0xffffff;
			jpUpgradeDescription.wordWrap = true;
			jpUpgradeDescription.width = 300;
			if (MenuWorld.language == 1) { addGraphic(jpUpgradeDescription); } else { addGraphic(upgradeDescription); }

			upgradeLevelDescription.x = 400;
			upgradeLevelDescription.y = 360;
			upgradeLevelDescription.scrollX = 0;
			upgradeLevelDescription.scrollY = 0;
			upgradeLevelDescription.color = 0xffffff;
			jpUpgradeLevelDescription.font = "defaultjpn";
			jpUpgradeLevelDescription.x = 400;
			jpUpgradeLevelDescription.y = 360;
			jpUpgradeLevelDescription.scrollX = 0;
			jpUpgradeLevelDescription.scrollY = 0;
			jpUpgradeLevelDescription.color = 0xffffff;
			if (MenuWorld.language == 1) { addGraphic(jpUpgradeLevelDescription); } else { addGraphic(upgradeLevelDescription); }

			upgradeNameDescription.x = 400;
			upgradeNameDescription.y = 340;
			upgradeNameDescription.scrollX = 0;
			upgradeNameDescription.scrollY = 0;
			upgradeNameDescription.color = 0xffff00;
			jpUpgradeNameDescription.font = "defaultjpn";
			jpUpgradeNameDescription.x = 400;
			jpUpgradeNameDescription.y = 340;
			jpUpgradeNameDescription.scrollX = 0;
			jpUpgradeNameDescription.scrollY = 0;
			jpUpgradeNameDescription.color = 0xffff00;
			if (MenuWorld.language == 1) { addGraphic(jpUpgradeNameDescription); } else { addGraphic(upgradeNameDescription); }

			levelPointsText.x = 400;
			levelPointsText.y = 470;
			levelPointsText.scrollX = 0;
			levelPointsText.scrollY = 0;
			levelPointsText.color = 0xffffff;
			jpLevelPointsText.font = "defaultjpn";
			jpLevelPointsText.x = 400;
			jpLevelPointsText.y = 470;
			jpLevelPointsText.scrollX = 0;
			jpLevelPointsText.scrollY = 0;
			jpLevelPointsText.color = 0xffffff;
			if (MenuWorld.language == 1) { addGraphic(jpLevelPointsText); } else { addGraphic(levelPointsText); }

			levelText.x = 400;
			levelText.y = 450;
			levelText.scrollX = 0;
			levelText.scrollY = 0;
			levelText.color = 0xffffff;
			jpLevelText.font = "defaultjpn";
			jpLevelText.x = 400;
			jpLevelText.y = 450;
			jpLevelText.scrollX = 0;
			jpLevelText.scrollY = 0;
			jpLevelText.color = 0xffffff;
			if (MenuWorld.language == 1) { addGraphic(jpLevelText); } else { addGraphic(levelText); }

			progressText.x = 400;
			progressText.y = 490;
			progressText.scrollX = 0;
			progressText.scrollY = 0;
			progressText.color = 0xffffff;
			jpProgressText.font = "defaultjpn";
			jpProgressText.x = 400;
			jpProgressText.y = 490;
			jpProgressText.scrollX = 0;
			jpProgressText.scrollY = 0;
			jpProgressText.color = 0xffffff;
			if (MenuWorld.language == 1) { addGraphic(jpProgressText); } else { addGraphic(progressText); }

			distanceText.x = 400;
			distanceText.y = 510;
			distanceText.scrollX = 0;
			distanceText.scrollY = 0;
			distanceText.color = 0xffffff;
			jpDistanceText.font = "defaultjpn";
			jpDistanceText.x = 400;
			jpDistanceText.y = 510;
			jpDistanceText.scrollX = 0;
			jpDistanceText.scrollY = 0;
			jpDistanceText.color = 0xffffff;
			if (MenuWorld.language == 1) { addGraphic(jpDistanceText); } else { addGraphic(distanceText); }

			cursorImage.scrollX = 0;
			cursorImage.scrollY = 0;
			addGraphic(cursorImage);
			
			spritemap.x = 400;
			spritemap.y = 120;
			spritemap.scrollX = 0;
			spritemap.scrollY = 0
			addGraphic(spritemap);
			spritemap.visible = false;
		}

		public function show():void {
			visible = true;
			if (MenuWorld.language == 1) { selectedIndex = Upgrade.jpUpgrades.length; } else { selectedIndex = Upgrade.upgrades.length; }
			firstFrame = true;
			confirmSfx.play();
			
			storeSfx.loop();
		}

		public function hide():void {
			visible = false;
			GameWorld.world().unpause();
			confirmSfx.play();
			
			storeSfx.stop();
		}

		public static function resetUpgrades():void {
			if (MenuWorld.language == 1) {
				for each (var jpUpgrade:Upgrade in Upgrade.jpUpgrades) {
					jpUpgrade.owned = false;
				}
			} else {
				for each (var upgrade:Upgrade in Upgrade.upgrades) {
					upgrade.owned = false;
				}
			}
		}

		public override function update():void {
			if (!visible) {
				return;
			}

			//if (Input.pressed(Key.ESCAPE)) {
//				hide();
			//}

			if (Input.pressed(Key.UP)) {
				selectedIndex--;
				cursorSfx.play();
			}
			if (Input.pressed(Key.DOWN)) {
				selectedIndex++;
				cursorSfx.play();
			}

			if (MenuWorld.language == 1) { selectedIndex = (selectedIndex + Upgrade.jpUpgrades.length + 1) % (Upgrade.jpUpgrades.length + 1); }
			else { selectedIndex = (selectedIndex + Upgrade.upgrades.length + 1) % (Upgrade.upgrades.length + 1); }

			if (Input.pressed("attack") || Input.pressed(Key.ENTER)) {
				if (!firstFrame) {
					if (selectedIndex == Upgrade.upgrades.length) {
						hide();
					} else {
						buyUpgrade();
					}
				}
			}

			cursorImage.x = kUpgradeStartX - 20;
			cursorImage.y = selectedIndex * kUpgradeHeight + kUpgradeStartY;

			for (var i:int = 0; i < Upgrade.upgrades.length; ++i) { // [A-zu-ra] Could probably get away with this;
				if (MenuWorld.language == 1) {						// jpUgrades and upgrades are the same length, after all.
					if (Upgrade.jpUpgrades[i].owned) {
						checkmarks[i].visible = true;
					} else {
						checkmarks[i].visible = false;
					}

					if (Upgrade.jpUpgrades[i].owned || Upgrade.jpUpgrades[i].canBuy()) {
						upgradeNameTexts[i].color = 0xffffff;
					} else {
						upgradeNameTexts[i].color = 0x808080;
					}
				} else {
					if (Upgrade.upgrades[i].owned) {
						checkmarks[i].visible = true;
					} else {
						checkmarks[i].visible = false;
					}

					if (Upgrade.upgrades[i].owned || Upgrade.upgrades[i].canBuy()) {
						upgradeNameTexts[i].color = 0xffffff;
					} else {
						upgradeNameTexts[i].color = 0x808080;
					}
				}
			}

			var player:Player = FP.world.getInstance("player") as Player;

			if (selectedIndex == Upgrade.upgrades.length) {
				upgradeNameDescription.text = "Up/Down to select an upgrade\nEnter to buy";
				upgradeNameDescription.color = 0xffffff;
				upgradeLevelDescription.text = "";
				upgradeDescription.text = "";
				jpUpgradeNameDescription.text = "↑↓でスキルを選択\nENTERキーで決定";
				jpUpgradeNameDescription.color = 0xffffff;
				jpUpgradeLevelDescription.text = "";
				jpUpgradeDescription.text = "";
				spritemap.visible = false;
			} else {
				upgradeNameDescription.text = Upgrade.upgrades[selectedIndex].name;
				upgradeDescription.text = Upgrade.upgrades[selectedIndex].description;
				upgradeNameDescription.color = 0xffff00;
				upgradeLevelDescription.text = "Required level: " + Upgrade.upgrades[selectedIndex].levelRequired;
				jpUpgradeNameDescription.text = Upgrade.jpUpgrades[selectedIndex].name;
				jpUpgradeDescription.text = Upgrade.jpUpgrades[selectedIndex].description;
				jpUpgradeNameDescription.color = 0xffff00;
				jpUpgradeLevelDescription.text = "必要なレベル　lv" + Upgrade.upgrades[selectedIndex].levelRequired;
				if (MenuWorld.language == 1) {
					if (Upgrade.jpUpgrades[selectedIndex].levelRequired > player.level) {
						jpUpgradeLevelDescription.color = 0xFF0000;
					} else {
						jpUpgradeLevelDescription.color = 0xFFFFFF;
					}
				} else {
					if (Upgrade.upgrades[selectedIndex].levelRequired > player.level) {
						upgradeLevelDescription.color = 0xFF0000;
					} else {
						upgradeLevelDescription.color = 0xFFFFFF;
					}
				}
				
				spritemap.visible = true;
				switch (selectedIndex) {
					case 11:
						spritemap.setFrame(0, 0);
						break;
					case 4:
					case 17:
					case 18:
						spritemap.setFrame(1, 0);
						break;
					case 10:
					case 20:
						spritemap.setFrame(2, 0);
						break;
					case 6:
					case 7:
					case 8:
					case 9:
						spritemap.setFrame(3, 0);
						break;
					case 19:
						spritemap.setFrame(4, 0);
						break;
					case 13:
					case 15:
						spritemap.setFrame(0, 1);
						break;
					case 14:
						spritemap.setFrame(1, 1);
						break;
					case 12:
						spritemap.setFrame(2, 1);
						break;
					case 5:
						spritemap.setFrame(3, 1);
						break;
					case 0:
					case 1:
					case 2:
					case 3:
					case 16:
						spritemap.setFrame(4, 1);
						break;
				}
			}

			levelText.text = "Current level: " + player.level;
			levelPointsText.text = "Skill points left: " + player.levelPoints;
			jpLevelText.text = "現在のレベル　lv" + player.level;
			jpLevelPointsText.text = "残りのスキルポイント　" + player.levelPoints;
			if (player.levelPoints <= 0) {
				if (MenuWorld.language == 1) { jpLevelPointsText.color = 0xff0000; } else { levelPointsText.color = 0xff0000; }
			} else {
				if (MenuWorld.language == 1) { jpLevelPointsText.color = 0xffffff; } else { levelPointsText.color = 0xffffff; }
			}
			
			var progress:int = player.progress() * 100.0;
			progressText.text = "Game progress: " + progress + "%";
			jpProgressText.text = "成功率　" + progress + "％";
			
			var thex:int = player.x - 200;
			if (thex < 0) {
				thex = 0;
			}
			distanceText.text = "" + (thex / 5000.0) + " miles traveled";
			jpDistanceText.text = "距離　" + (thex / 3200.0) + "㎞";

			super.update();
			
			firstFrame = false;
		}

		private function buyUpgrade():void {
			if (MenuWorld.language == 1) {
				if (!Upgrade.jpUpgrades[selectedIndex].canBuy() || Upgrade.jpUpgrades[selectedIndex].owned) {
					// Can't buy.
					errorSfx.play();
				} else {
					var jpPlayer:Player = FP.world.getInstance("player") as Player;
					var jphpPercent:Number = (jpPlayer.health as Number) / Upgrade.maxHealth();
					jpPlayer.levelPoints--;
					Upgrade.jpUpgrades[selectedIndex].owned = true;
					levelupSfx.play();
					jpPlayer.health = jphpPercent * Upgrade.maxHealth();
				}
			} else {
				if (!Upgrade.upgrades[selectedIndex].canBuy() || Upgrade.upgrades[selectedIndex].owned) {
					// Can't buy.
					errorSfx.play();
				} else {
					var player:Player = FP.world.getInstance("player") as Player;
					var hpPercent:Number = (player.health as Number) / Upgrade.maxHealth();
					player.levelPoints--;
					Upgrade.upgrades[selectedIndex].owned = true;
					levelupSfx.play();
					player.health = hpPercent * Upgrade.maxHealth();
				}

			}
		}
	}
	
	// [A-zu-ra] This was probably the toughest part of the translation to shimmy in. idk, i'm bad at optimization orz
}
