package {
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Messages extends Entity {
		private static const kNumber:int = 10;
		
		private var texts:Vector.<Text> = new Vector.<Text>();
		
		public function Messages():void {
			for (var i:int = 0; i < kNumber; ++i) {
				var text:Text = new Text("", 0, i * 15);
				text.scrollX = 0;
				text.scrollY = 0;
				if (i == 0) {
					text.color = 0xffffff;
				} else {
					text.color = 0x808080;
				}
				if (MenuWorld.language == 1) { text.font = "defaultjpn"; }
				texts.push(text);
				addGraphic(text);
			}
			x = 30;
			y = 30;
			
			layer = -800;
			name = "messages";
		}
		
		public static function message(message:String):void {
			var instance:Messages = FP.world.getInstance("messages");
			for (var i:int = kNumber - 2; i >= 0; --i) {
				instance.texts[i+1].text = instance.texts[i].text;
			}
			instance.texts[0].text = message;
		}
	}
}
