package {
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class HeartPowerup extends Powerup {
		[Embed(source='../img/heartpowerup.png')]
		private static const kImageFile:Class;
		private var image:Image = new Image(kImageFile);
		
		[Embed(source='../sfx/heart_pickup.mp3')]
		private static const kSfxFile:Class;
		private var sfx:Sfx = new Sfx(kSfxFile);

		public function HeartPowerup() {
			setHitbox(image.width, image.height);
			image.originX = halfWidth;
			image.originY = height;
			originX = halfWidth;
			originY = height;
			graphic = image;
		}
		
		public function reset(x:Number, y:Number):void {
			this.x = x;
			this.y = y;
		}
		
		override protected function onPickup():void {
			var player:Player = world.getInstance("player") as Player;
			player.getHealth();
			GameWorld.bonesFound++;
			sfx.play();
		}
	}
}
